# Frankensense  
A temperature sensor thrown together out of an ESP8266 module and a DS18B20.

Now with MQTT.

It replies over HTTP with a JSON formatted sensor value.  
This is also published over MQTT on every sensor read (currently every 800ms)

## Useage  
Copy over the sample\_config.h to 82\_b20\_mqtt\_json/config.h and fill in your wifi credentials.  
