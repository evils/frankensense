// firmware for an ESP8266 to read a DS18B20,
// convert the data to celcius at the sensor's speed,
// send the data out via MQTT
// and http in json format

// based on:
// - https://gitlab.com/evils/frankensense/tree/old_json_only
// - https://github.com/astolzen/mqtt_esp8266_ds1820_arduino

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <OneWire.h>
#include <PubSubClient.h>

#include "Arduino.h"
#include "config.h"

String clientName;

/*************************/
/* optional serial debug */
/*************************/

#ifdef DEBUG

	#define DEBUG_PRINT(x) Serial.print(x)
	#define DEBUG_PRINTLN(x) Serial.println(x)

#else

	#define DEBUG_PRINT(x)
	#define DEBUG_PRINTLN(x)

#endif

/***************/
/* Temperature */
/***************/

OneWire ds(sensor_data); // Temperature sensor

// time of last temperature update
unsigned long temp_ct;
unsigned long temp_update_time = 0;


// initial value
float temp_celcius = 0;

byte temp_addr[8];
byte temp_type_s;

void temp_init() {
retry:
	if (!ds.search(temp_addr)) {

		Serial.println("No more addresses.");
		Serial.println();
		ds.reset_search();
		delay(1000);
		goto retry;
	}
	if (OneWire::crc8(temp_addr, 7) != temp_addr[7]) {
		Serial.println("CRC is not valid!");
		delay(1000);
		goto retry;
	}
	// the first ROM byte indicates which chip
	switch (temp_addr[0]) {

		case 0x10:
			temp_type_s = 1;
			break;
		case 0x28:
			temp_type_s = 0;
			break;
		case 0x22:
			temp_type_s = 0;
			break;
		default:
			return;
	}
}

void temp_request() {
	ds.reset();
	ds.select(temp_addr);
	// start conversion, with parasite power on at the end
	ds.write(0x44, 1);
}

// Should be run 750ms or more after temp_request()
// (see sensor_interval)
void temp_read() {
	byte i;
	byte data[12];

	ds.reset();
	ds.select(temp_addr);
	ds.write(0xBE); // Read Scratchpad

	for (i = 0; i < 9; i++) { // we need 9 bytes

		data[i] = ds.read();
	}

	// Convert the data to actual temperature
	// because the result is a 16 bit signed integer, it should
	// be stored to an "int16_t" type, which is always 16 bits
	// even when compiled on a 32 bit processor.
	int16_t raw = (data[1] << 8) | data[0];

	// TODO base this on sensor_interval?
	if (temp_type_s) {

		raw = raw << 3; // 9 bit resolution default
		if (data[7] == 0x10) {

			// "count remain" gives full 12 bit resolution
			raw = (raw & 0xFFF0) + 12 - data[6]; }
	}
	else {
		byte cfg = (data[4] & 0x60);

		// at lower res, the low bits are undefined,
		// so let's zero them
		if (cfg == 0x00)
			raw = raw & ~7;  // 9 bit resolution, 93.75 ms
		else if (cfg == 0x20)
			raw = raw & ~3; // 10 bit res, 187.5 ms
		else if (cfg == 0x40)
			raw = raw & ~1; // 11 bit res, 375 ms
		// default is 12 bit resolution, 750 ms conversion time
	}
	temp_celcius = (float) raw / 16.0;
}


/********/
/* MQTT */
/********/

WiFiClient wifiClient;

void callback(char* topic, byte* payload, unsigned int length) {
	// handle message arrived
}

PubSubClient client(mqtt_server, 1883, callback, wifiClient);

String macToStr(const uint8_t* mac)
{
	String result;
	for (int i = 0; i < 6; ++i) {
		result += String(mac[i], 16);
		if (i < 5)
			result += ':';
	}
	return result;
}

bool mqtt_connect(String clientName) {
  client.disconnect();
  if (client.connect((char*) clientName.c_str())) {
    Serial.println("Connected to MQTT broker");
    return true;
  }
  else {
    Serial.println("MQTT connect failed");
    return false;
  }
}

void mqtt_publish(String topic, String payload) {

        if (client.connected()){

		DEBUG_PRINT("Sending payload: ");
		DEBUG_PRINT(payload);
		DEBUG_PRINT(", To topic: ");
		DEBUG_PRINT(topic);
        }

        if (client.publish((char*) topic.c_str(),
                           (char*) (payload).c_str())) {

                DEBUG_PRINTLN(", Publish: ok");
        }
        else {
                Serial.println(", Publish: failed, attempting reconnect");
                mqtt_connect(clientName);
        }
}


/********/
/* HTTP */
/********/

// declare a server
ESP8266WebServer server(80);

// formulation of the "web page"
void handleRoot() {
	server.send(200, "text/plain", "{\"t1\":\""
		+ ((String) temp_celcius + "\"}"));

		DEBUG_PRINTLN("HTTP served");
}


/******************************************/
/* Setup and loop to glue things together */
/******************************************/

void setup() {

	// power sensor from digital pin (don't do this with many sensors)
	// TODO figure out parasitic power
	pinMode(sensor_power, OUTPUT);
	digitalWrite(sensor_power, HIGH);

	// TODO figure out if this is needed
	pinMode(sensor_data, INPUT_PULLUP);


	Serial.begin(115200);

	temp_init();

	Serial.println();
	Serial.println("Connecting WiFi");


	/********/
	/* WiFi */
	/********/

	WiFi.begin(ssid, password);
	Serial.println("");

	// Wait for connection
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());


	/********/
	/* HTTP */
	/********/

	server.begin();
	Serial.println("HTTP server started");

	server.on("/", handleRoot);


	/********/
	/* MQTT */
	/********/

	// Generate client name based on MAC address
	//and last 8 bits of microsecond counter
	// TODO should this change?
	clientName += "esp8266-";
	uint8_t mac[6];
	WiFi.macAddress(mac);
	clientName += macToStr(mac);
	clientName += "-";
	clientName += String(micros() & 0xff, 16);

	Serial.print("Connecting to ");
	Serial.print(mqtt_server);
	Serial.print(" as ");
	Serial.println(clientName);

 if (!mqtt_connect(clientName)) {
    Serial.println("Will reset and try again...");
    abort();
 }

// discard first reading
temp_request();
delay(sensor_interval);
// request again, will be read sensor_interval later in loop
temp_request();
temp_update_time = millis();

}

void loop() {

	// read temperature every sensor_interval
	temp_ct = millis();
	if (temp_ct - temp_update_time > sensor_interval) {
		temp_update_time = temp_ct;
		temp_read();
		// and publish to mqtt
		mqtt_publish(topic, (String)temp_celcius);
		// request sensor to calculate the value,
		// can be read 1 sensor_interval later
		temp_request();
	}

	// Deal with HTTP clients
	server.handleClient();
}
