#pragma once

// this is a sample config.h,
// fill in your credentials and server
// and put this in the sketch folder as config.h


// uncomment this define to
// enable debug messages on serial
#define DEBUG

// wifi credentials
const char* ssid = "Your_wifi_network";
const char* password = "Your_wifi_pass";

// mqtt server
const char* mqtt_server = "Your_mqtt_broker";
const String topic = "temperature";

// GPIO pin of sensor data (OneWire)
const int sensor_data = 2;
// GPIO pin of sensor power (+3 - +5.5Vdc)
const int sensor_power = 0;

// sensor (as currently set) takes 750ms to read
// so interval has to be >= that
const int sensor_interval = 800;
